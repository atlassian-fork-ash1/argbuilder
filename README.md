# Argbuilder

## Usage

```
$ python argbuilder.py -h
usage: argbuilder.py [-h] [--type {sysprops,java_opts,both}]
                     [--confdir CONFDIR] [--platform {unicorn,vertigo,dev}]
                     [--debug]

Parses tree of properties files and prints out an argument string for running
confluence. NOTE: Sysprops files will merge, java_opts files will reset and
override

optional arguments:
  -h, --help            show this help message and exit
  --type {sysprops,java_opts,both}
                        Type of arguments to build
  --confdir CONFDIR     Location of config to turn into arguments
  --platform {unicorn,vertigo,dev}
                        Type of arguments to build
  --debug               Print debug info to stderr
```

### Configuration

* Create a `runconf` directory
* Copy `argbuilder-config.yaml.example` to `runconf/argbuilder-config.yaml` and update the hierarchies as required -- file patterns higher in the array have greater precedence
* Create a tree of properties files under `runconf`, eg. `runconf/sysprops/vertigo/common.properties`

#### Env_map

The env map config hash is used to map environment variables to tokens in the hierarchy strings.

```
# example from argbuilder-config.yaml
env_map:
  unicorn:
    environment: "UNICORN_APPLICATION_ENVIRONMENT"
    rack: "UNICORN_AREA"
    zone: "UNICORN_ZONE"
hierarchy:
  unicorn:
    - "%{::platform}/zones/%{::zone}/environments/%{::environment}/racks/%{::rack}"
    - "%{::platform}/common"
    - "global"
```

For example, platform is special and comes from a cli argument, however eg. `%{::zone}` would be mapped to `UNICORN_ZONE` and so on and so forth.

### Properties files

Example properties file:

```
# A comment
magic.number=42

feature.enabled=true
feature.env.password=env:///SECRET_PASSWORD

# Config with spaces, etc should be surrounded by quotes
feature.settings="one two three"
```

Note the `env:///ENVIRONMENT_VARIABLE` syntax.  This will set the value of the property to be the value of the environment if available, or discard the property if not.  For simplicity, argbuilder will only scan for the first environment variable. eg.

```
# global.properties
feature.a=env:///VALUE_ONE
feature.b=env:///VALUE_TWO extra text
feature.c=  env:///VALUE_THREE
feature.d=env:///VALUE_FOUR env:///VALUE_FIVE
feature.e=env:///VALUE_NOT_SET_IN_ENV

# running argbuilder with corresponding ENV vars
VALUE_ONE=1 VALUE_TWO=2 VALUE_THREE=3 VALUE_FOUR=4 VALUE_FIVE=5 python argbuilder.py <...>

# Would result in:
-Dfeature.a=1 -Dfeature.b=2 -Dfeature.c=3 -Dfeature.d=4
```

## Contributing

Please make sure any modifications to argbuilder work in both python 2
and python 3.

## Testing

### Unit tests
```bash
# Setup a virtualenv and run tests
./test_argbuilder/run-tests.sh
```

### Manual testing

You can manually verify argbuilder output like so:

```
UNICORN_APPLICATION_ENVIRONMENT=jirastudio-dev UNICORN_AREA=rack-101 UNICORN_ZONE=sc2 python argbuilder.py --confdir runconf --platform unicorn
```
