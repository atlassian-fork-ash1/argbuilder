from argparse import ArgumentParser
import os
import pprint
import re
import sys
import yaml

pp = pprint.PrettyPrinter(indent=1, stream=sys.stderr)

SYSPROP_OVERRIDES = {
    # 'com.atlassian.labs.jolokia.servlet.AtlassianJolokiaServlet.policy': '/data/confluence/config/jolokia-access.xml',
    # 'confluence.attachment.data.mode': 'CLOUD_ONLY',
    # 'confluence.cloud.conversion.data.mode': 'MEDIA_ONLY',
}
# See https://pug.jira-dev.com/wiki/display/CC/Confluence+Icebat+Config+Categorisation
IGNORED_SYSPROPS = [
    'atlassian.confluence.plugin.roster.file',
    'atlassian.license.location',
    'atlassian.plugins.enable.wait',
    'atlassian.tenancy.enabled',
    'com.atlassian.labs.jolokia.servlet.AtlassianJolokiaServlet.policy',
    'com.atlassian.plugin.loaders.RosterFilePluginLoader.referenceMode',
    'http.nonProxyHosts',
    'http.proxyHost',
    'http.proxyPort',
    'https.proxyHost',
    'https.proxyPort',
    'instance.id',
    'skip.plugin.system.shutdown',
    'studio.env',
    'studio.initial.data.xml',
    'studio.webdav.directory',
    'unicorn.dc',
    'unicorn.instance',
    'unicorn.rack',
]
COMMON_SYSPROPS = [
    'STUDIO_COMPONENT_APP',
    'activeobjects.servicefactory.config.timeout',
    'aid.avatar.url',
    'aid.profile.url',
    'atlassian.connect.userimpersonation.enabled',
    'atlassian.connect.userimpersonation.preferred',
    'atlassian.connect.userprovisioning.mode',
    'atlassian.darkfeature.confluence.baseurl.cdn.enabled',
    'atlassian.darkfeature.confluence.sidebar.deferred',
    'atlassian.darkfeature.confluence.vertigo.space.finder',
    'atlassian.darkfeature.unified.usermanagement.licensing.users.disable',
    'atlassian.darkfeature.unified.usermanagement',
    'atlassian.dev.confluence.whatsnew.show',
    'atlassian.feature.flag.service.disable.redis.store',
    'atlassian.feature.flag.service.offline.mode',
    'atlassian.filestore.external.url',
    'atlassian.filestore.url',
    'atlassian.upm.on.demand.pva.blacklist',
    'atlassian.upm.on.demand',
    'atlassian.upm.preinstalled.disableable.override',
    'atlassian.upm.user.installed.p2.override',
    'atlassian.use.environment.variables',
    'com.sun.management.jmxremote.authenticate',
    'confluence.attribute.listener.enabled',
    'confluence.hibernate.querycache.disabled',
    'confluence.pdfexport.permits.size',
    'confluence.restricted.groups',
    'confluence.user.propertyset.metricsLogging.freq',
    'confluence.vcache.metricsLogging.freq',
    'file.encoding',
    'java.awt.headless',
    'java.security.egd',
    'javax.net.ssl.trustStore',
    'notifications.sender.thread.count',
    'officeconnector.converter.permits.size',
    'officeconnector.importer.permits.size',
    'officeconnector.pdf.memory.guard.enabled',
    'org.apache.jasper.runtime.BodyContentImpl.LIMIT_BUFFER',
    'pas.endpoint',
    'reindex.attachments.thread.count',
    'studio.home',
    'studio.socks.host',
    'studio.socks.port',
    'thumbnail.generator.permits.size',
    'upm.pac.disable',
    # delete the following in the future
    'SCHEDULED_JOB_HISTORY_DISABLED',
    'confluence.skip.reindex',
    'ehcache.stats.compaction',
    'hibernate.state.compression',
]
# Unicorn specific properties:
# 'atlassian.logging.cloud.enabled',
# 'atlassian.logging.service.id',
# 'memcached.host',
# 'memcached.port',
# TODO: jolokia-access.xml
GLOBAL_COMMON_PROPERTIES_FILE = '%s/%s/global.properties'   # self.output_dir, properties_type
PLATFORM = 'unicorn'
YAML_KEYS = [
    'max_heap',
    'max_heap_free_ratio',
    'min_heap',
    'new_ratio',
    'java_opts',
    'sysprops',
    'vm_flavour',
]


class IcebatConverter:
    def __init__(self, icebat_dir, output_dir, debug, dry_run):
        self.icebat_dir = icebat_dir
        self.output_dir = output_dir
        self.debug = debug
        self.dry_run = dry_run

        self.hieradata_dir = '%s/hieradata' % icebat_dir

        self.hieradata_files = self.get_hieradata_files()
        if self.debug:
            print >> sys.stderr, '=> Hieradata Files:'
            pp.pprint(self.hieradata_files)
            pass

    def get_hieradata_files(self):
        hieradata_files = []

        # Get list of hieradata yaml files that aren't symlinks
        for root, dirs, files in os.walk(self.hieradata_dir):
            for file in files:
                if file.endswith('.yaml') and os.path.isfile(os.path.join(root, file)):
                    hieradata_files.append(os.path.join(root, file))

        return hieradata_files

    def outfile_path(self, yaml_file, properties_type):
        return re.sub(
            r'^.*hieradata/(.*).yaml',
            r'%s/%s/%s/\1.properties' % (self.output_dir, properties_type, PLATFORM),
            yaml_file
        )

    def makedir(self, path):
        if not os.path.exists(path):
            if self.dry_run:
                print 'Would create directory %s' % path
            else:
                if self.debug:
                    print >> sys.stderr, 'Creating directory %s' % path

                os.makedirs(path)

    def convert_type(self, file, properties, type, common_java_properties=None):
        # Are there any properties of type?
        if type in properties.keys() and properties[type]:
            # HACK to merge java_opts. This may break on certain type of options
            if type == 'java_opts' and 'common.yaml' not in file and common_java_properties is not None:
                merged_props = common_java_properties
                for k, v in properties['java_opts'].items():
                    print 'Merging java_opt: %s => %s from %s' % (k, v, file)
                    merged_props[k] = v
                properties['java_opts'] = merged_props
            # END HACK

            outfile = self.outfile_path(file, type)
            outfile_dir = os.path.dirname(outfile)
            self.makedir(outfile_dir)

            if self.dry_run:
                print 'Would write properties to %s' % outfile
            else:
                self.write_properties(properties[type], type, outfile)

    def convert(self):
        # TODO: add special cases
        #  Hack for merging java_opts
        common_java_opts = {}
        for file in self.hieradata_files:
            properties = get_properties(file)

            # Are there properties of any type?
            if properties:
                # Hack for merging java_opts
                if 'common.yaml' in file:
                    common_java_opts = properties['java_opts']

                self.convert_type(file, properties, 'java_opts', common_java_properties=common_java_opts)
                self.convert_type(file, properties, 'sysprops')

            else:
                if self.debug:
                    print >> sys.stderr, 'No properties for %s' % file

    def write_properties(self, properties, type, outfile):
        global_common_properties_file = None
        cf = None
        if 'common.properties' in outfile:
            global_common_properties_file = GLOBAL_COMMON_PROPERTIES_FILE % (self.output_dir, type)
            cf = open(global_common_properties_file, 'w')

        with open(outfile, 'w') as f:
            for k, v in properties.items():
                if 'exclude' in v and v['exclude']:
                    continue

                if type == 'java_opts':
                    if not v['value']:
                        print '%s: %s' % (outfile, v['property'])
                        print >> f, '%s' % v['property']
                    else:
                        print '%s: %s=%s' % (outfile, v['property'], v['value'])
                        print >> f, '%s=%s' % (v['property'], v['value'])
                elif type == 'sysprops':
                    if not v['property'] in IGNORED_SYSPROPS:
                        if 'common.properties' in outfile and v['property'] in COMMON_SYSPROPS:
                            print '%s: %s=%s' % (outfile, v['property'], v['value'])
                            print >> cf, '%s=%s' % (v['property'], v['value'])

                        else:
                            print '%s: %s=%s' % (outfile, v['property'], v['value'])
                            print >> f, '%s=%s' % (v['property'], v['value'])

        # Close global common props file
        if cf is not None:
            cf.close()


def get_properties(file):
    properties = {}
    with open(file, 'r') as data:
        doc = yaml.load(data)
        for key in YAML_KEYS:
            if key in ['java_opts', 'sysprops']:
                if 'confluence::%s' % key in doc.keys():
                    properties[key] = doc['confluence::%s' % key]

    # pp.pprint(properties)

    return properties


def main():
    parser = ArgumentParser(
        description=(
            'Parses tree of properties files and prints out an argument string'
            ' for running confluence.  NOTE: Sysprops files will merge, '
            'java_opts files will reset and override'
        )
    )
    parser.add_argument(
        'icebat_dir',
        help='Location of icebat repository'
    )
    parser.add_argument(
        'output_dir',
        help='Directory for properties file tree output'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Print debug info to stderr'
    )
    parser.add_argument(
        '--dry-run',
        action='store_true',
        help='Print debug info to stderr'
    )

    # TODO: add ability to do a single properties file
    arguments = parser.parse_args()
    icebat_dir = arguments.icebat_dir
    output_dir = arguments.output_dir

    if arguments.debug:
        debug = True
    else:
        debug = False

    if arguments.dry_run:
        dry_run = True
    else:
        dry_run = False

    ibc = IcebatConverter(icebat_dir, output_dir, debug, dry_run)
    ibc.convert()

if __name__ == '__main__':
    main()
